#ifndef NUMERIC_TABLE_H_LKJ4
#define NUMERIC_TABLE_H_LKJ4

#include "Table.h"
#include <vector>
#include <map>
//#include <algorithm>

class NumericTable : public Table {

  public:
    NumericTable ();

    void add_value (std::string key, double value);
    void write_text (std::ostream &output) const;
    void write_tex  (std::ostream &output) const;

  private:
//    std::vector<std::string> columnHeaders;
    std::map<std::string,std::vector<double> > tableEntries;

};

#endif // NUMERIC_TABLE_H_LKJ4

