#ifndef TABLE_H_2LK3
#define TABLE_H_2LK3

#include <string.h>
#include <iostream>

class Table {

  public:
    /*
     *
     */
    virtual void add_value (std::string key, double value) = 0;
    /*
     *
     */
    virtual void write_text (std::ostream &output) const = 0;
    /*
     *
     */
    virtual void write_tex (std::ostream &output) const = 0;


};

#endif // TABLE_H_2LK3
 
