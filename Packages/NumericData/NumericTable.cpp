#include "NumericTable.h"

NumericTable::NumericTable () {

}

void NumericTable::add_value (std::string key, double value) {
 
  if (tableEntries.find(key) == tableEntries.end())
    tableEntries[key] = std::vector<double>();

  tableEntries[key].push_back(value);
 
}

void NumericTable::write_text (std::ostream &output) const {
  unsigned int maxColumnLength = 0;

  std::map<std::string,std::vector<double> >::const_iterator it;
  for (it = tableEntries.begin(); it != tableEntries.end(); ++it){
    output << it->first << "  ";
    if (it->second.size() > maxColumnLength) maxColumnLength = it->second.size();
  } output << std::endl;

  for (unsigned int i = 0; i < maxColumnLength; i++){
    for (it = tableEntries.begin(); it != tableEntries.end(); it++){
      if (it->second.size() > i)
        output << it->second[i] << "  ";
      else
        output << "    " << "  ";
    } output << std::endl;
  }

}

void NumericTable::write_tex  (std::ostream &output) const {

}




