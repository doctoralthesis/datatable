#include "../NumericTable.h"


int main (){

  NumericTable table;

  table.add_value("first", 1.3);
  table.add_value("first", 1.3);
  table.add_value("first", -1.4);
  table.add_value("second", 5.7);
  table.add_value("third", 4.9);
  table.add_value("third", 4.9);
  table.add_value("third", 4.9);
  table.add_value("third", 4.9);

  table.write_text(std::cout);
  std::cout << std::endl;
  table.write_tex (std::cout);



  return 0;

}

